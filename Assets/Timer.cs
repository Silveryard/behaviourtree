﻿using UnityEngine;
using System.Collections;

public class Timer : Leaf{
    public float Time;

    private float elapsed = 0.0f;
    
    public override IEnumerator Run(){

        while (elapsed < Time){
            elapsed += UnityEngine.Time.deltaTime;

            yield return Result.Running;
        }

        elapsed = 0.0f;
        yield return Result.Success;
    }
}
