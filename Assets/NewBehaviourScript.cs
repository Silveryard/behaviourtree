﻿using UnityEngine;
using System.Collections;

public class NewBehaviourScript : Leaf{
    public bool FolloWaypoint;

    public override IEnumerator Run(){
        if (FolloWaypoint)
            yield return Result.Success;
        else{
            yield return Result.Failure;
        }
    }
}
