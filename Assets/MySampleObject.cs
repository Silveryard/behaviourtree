﻿using UnityEngine;
using System.Collections;

public class MySampleObject : MonoBehaviour{

    public BehaviourTree Tree;

    protected void Start(){
        StartCoroutine(Tree.RootRoutine());
    }

}