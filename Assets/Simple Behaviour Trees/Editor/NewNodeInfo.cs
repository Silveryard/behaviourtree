﻿using System;

/// <summary>
/// Contains information required for creating a new node
/// </summary>
class NewNodeInfo
{
    /// <summary>
    /// The type of the node to create
    /// </summary>
    public Type T;

    /// <summary>
    /// The parent of the node to create
    /// </summary>
    public Node Parent;

    /// <summary>
    /// The type of the parent of the node to create
    /// </summary>
    public Type ParentType;
}
