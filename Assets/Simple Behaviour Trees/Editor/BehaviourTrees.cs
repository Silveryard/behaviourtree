﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using Object = System.Object;

/// <summary>
/// Contains Code to edit and create a BehaviourTree in an editor window
/// </summary>
public class BehaviourTrees : EditorWindow
{
    #region Attributes
    /// <summary>
    /// Sets the default Window width and height
    /// </summary>
    [SerializeField] 
    private Rect _windowSize = new Rect(0, 0, 200, 200);

    /// <summary>
    /// Holds an instance of the edited behaviour tree
    /// </summary>
    [SerializeField]
    private BehaviourTree _tree;

    /// <summary>
    /// Holds the value of the Textfield that sets the name of the new tree
    /// </summary>
    [SerializeField] 
    private String _treeName = "";

    /// <summary>
    /// Holds the connection between the windows and the actual nodes
    /// Gets rewritten every OnGUI-Call
    /// </summary>
    [SerializeField]
    private Dictionary<int, Node> _windows;
    
    /// <summary>
    /// Holds the required width for every node with their subnodes
    /// Gets rewritten every OnGUI-Call
    /// </summary>
    [SerializeField]
    private Dictionary<Node, int> _widthTable;
    
    /// <summary>
    /// Holds the current offset for moving around the behaviour tree
    /// </summary>
    [SerializeField]
    private Vector2 _offset = Vector2.zero;

    /// <summary>
    /// Holds the next WindowId. Gets resettet at beginning of OnGUI
    /// </summary>
    [SerializeField]
    private int _nextWindowId;
    #endregion

    #region Static
    /// <summary>
    /// Shows the BehaviourTrees-Window
    /// Creates a new one if none exists
    /// </summary>
    [MenuItem("Window/BehaviourTree")]
    public static void ShowWindow(){
        BehaviourTrees window = GetWindow<BehaviourTrees>();
        window.autoRepaintOnSceneChange = true;
    }
    #endregion

    #region Update
    protected void Update(){
        if (EditorApplication.isPlaying && _tree.TickEmpty()){
            _tree.Tick += OnTick;
        }
    }

    protected void OnTick(){
        Repaint();
    }
    #endregion

    #region GUI Stuff
    /// <summary>
    /// Holds the drawing logic
    /// </summary>
    protected void OnGUI()
    {
        /*
        * Check if the user whants to drag the tree
        */
        Event ev = Event.current;
        if (ev.type == EventType.MouseDrag)
        {
            if (ev.mousePosition.y > 20)
            {
                _offset += ev.delta;
            }
            ev.Use();
        }

        #region Right Panel
        GUILayout.BeginArea(new Rect(Screen.width - 200, 0, 200, 100));
        {
            EditorGUILayout.BeginVertical();
            {
                EditorGUILayout.BeginHorizontal();
                {
                    GUILayout.Label("Name: ");
                    /*
                     * Get the name of the new tree and save it
                     */
                    _treeName = GUILayout.TextField(_treeName, 20);
                }
                EditorGUILayout.EndHorizontal();

                #region BtnCreateNewTree
                if (GUILayout.Button("Create New Tree"))
                {
                    /*
                     * Create new tree
                     */
                    BehaviourTree tree = CreateInstance<BehaviourTree>();
                    /*
                     * Create a new Asset based on the typed in name
                     */
                    AssetDatabase.CreateAsset(tree, "Assets/" + _treeName + ".asset");
                    /*
                     * Save the asset
                     */
                    AssetDatabase.SaveAssets();
                    /*
                     * Clear the textbox
                     */
                    _treeName = "";
                }
                #endregion

                #region BtnResetView
                if (GUILayout.Button("Reset View"))
                {
                    /*
                     * Reset the offset
                     */
                    _offset = new Vector2();
                }
                #endregion
            }
            EditorGUILayout.EndVertical();
        }
        GUILayout.EndArea();
        #endregion

        #region Left Panel
        GUILayout.BeginArea(new Rect(0, 0, 150, 35));
        {
            GUILayout.Label("Place to edit:");
            _tree = (BehaviourTree)EditorGUILayout.ObjectField(_tree, typeof(BehaviourTree), false);
        }
        GUILayout.EndArea();
        #endregion

        #region Tree

        /*
         * If thres nothing to draw -> return
         */
        if (_tree == null) return;
        
        if (_tree.Active && !EditorApplication.isPlaying)
            DisableNode(_tree.ChildNode);

        /*
         * Calculate the required widths for the nodes
         */
        CalculateWidths();

        BeginWindows();
        /*
         * Reset parameters
         */
        _nextWindowId = 0;
        _windows = new Dictionary<int, Node>();

        /*
         * Draw the ChildNode. This will start a recursive call of DrawNode for every ChildNode
         */
        DrawNode(_tree, new Rect(Screen.width / 2f - _windowSize.width / 2f + _offset.x, 20 + _offset.y, _windowSize.width, _windowSize.height));
        EndWindows();
        #endregion
    }
    
    /// <summary>
    /// Draws a single node
    /// If decorator: Calls the appropiate Action to handle child nodes
    /// </summary>
    /// <param name="n">The node to draw</param>
    /// <param name="target">The target rect where to draw the node</param>
    private void DrawNode(Node n, Rect target)
    {
        /*
         * If theres nothing to draw -> return
         */
        if (n == null) return;

        /*
         * Get a new Id for the Window and add an assoziation to the node in _windows
         */
        int id = GetNextWindowId();
        _windows.Add(id, n);

        /*
         * Generate Window height based on the number of Fields
         */
        FieldInfo[] infos = n.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance);
        target.height = (infos.Length - 4) * 25 + 55;

        /*
         * Store default background color
         */
        Color def = GUI.backgroundColor;

        /*
         * If the parent is active, draw it green
         */
        if (n.Active)
            GUI.backgroundColor = Color.green;

        /*
         * Draw the Window
         */
        GUI.Window(id, target, DrawNodeWindow, n.GetType().ToString());

        /*
         * Reset backgroundcolor
         */
        GUI.backgroundColor = def;

        /*
         * If decorator Node:
         * Select the required method to draw ChildrenNode(s)
         */
        if (IsSingleChildNode(n))
        {
            DrawSingleChildNode((SingleChildNode)n, target);
        }
        else if (IsMultiChildNode(n))
        {
            DrawMultiChildNode((MultiChildNode)n, target);
        }
    }

    /// <summary>
    /// Handles drawing of ChildNode in a class derived from SingleChildNode
    /// </summary>
    /// <param name="parent">The parent node typeof(SingleChildNode)</param>
    /// <param name="parentRect">The drawn rect of the parent node</param>
    private void DrawSingleChildNode(SingleChildNode parent, Rect parentRect)
    {
        /* 
         * If collapsed -> Do not draw ChildNode
         */
        if (parent.Collapsed) return;

        /*
         * Calculate the Child rect based on its parent
         */
        Rect target = new Rect(parentRect.x, parentRect.y + parentRect.height + 50, parentRect.width, parentRect.height);

        /*
         * If theres nothing to draw -> skip drawing something
         */
        if (parent.ChildNode != null)
        {
            /*
             * Draw a Curve between Parent and Child to symbolise semantic connection
             */
            DrawNodeCurve(parentRect, target);
            /*
             * Now draw the ChildNode
             */
            DrawNode(parent.ChildNode, target);
        }
    }

    /// <summary>
    /// Handles drawing of ChildNodes in a class derived from MultiChildNode
    /// </summary>
    /// <param name="parent">The parenr node typeof(MultiChildNode)</param>
    /// <param name="parentRect">The drawn rect of the parent node</param>
    private void DrawMultiChildNode(MultiChildNode parent, Rect parentRect)
    {
        /*
         * If collapsed -> Do not draw ChildNodes
         */
        if (parent.Collapsed) return;

        Rect target = new Rect();

        /*
         * Draw every child in parent.ChildNodes
         */
        for (int i = 0; i < parent.ChildNodes.Count; i++)
        {
            /*
             * Get the required width of the parent from dictionary
             */
            int parentWidth = _widthTable[parent];

            /* 
             * Initialize xPos with parents position
             */
            float x = parentRect.x;

            /*
             * If theres more than one Child:
             * Calculate xPos for current Child
             */
            if (parent.ChildNodes.Count > 1){
                if (i == 0){
                    x -= parentWidth/2f - _windowSize.width / 2f;
                }
                if (i > 0){
                    x = target.x + _widthTable[parent.ChildNodes[i - 1]] / 2f + _widthTable[parent.ChildNodes[i]]/2f;
                }

                //x += (i*(parentWidth/parent.ChildNodes.Count)) - ((parent.ChildNodes.Count - 1) * parentRect.width / 2f);
            }

            /*
             * Generate the new Rect
             */
            target = new Rect(x, parentRect.y + parentRect.height + 50, parentRect.width, parentRect.height);

            /*
             * Draw a Curve between Parent and Child to symbolise semantic connection
             */
            DrawNodeCurve(parentRect, target);
            /*
             * Now draw the Child
             */
            DrawNode(parent.ChildNodes[i], target);
        }
    }

    /// <summary>
    /// Draws the window for a specific node
    /// </summary>
    /// <param name="id">The given WindowId from GUI.Window</param>
    private void DrawNodeWindow(int id)
    {
        #region BtnCollapse

        string btnText = _windows[id].Collapsed ? "Show" : "Collapse";
        if (!IsLeaf(_windows[id]) && GUILayout.Button(btnText))
        {
            _windows[id].Collapsed = !_windows[id].Collapsed;

            CalculateWidths();
        }

        #endregion

        #region Draw Fields
        /*
         * Holds information if the current node has any changed values
         */
        bool changed = false;
        /*
         * Holds information about every public attribute of the node
         */
        FieldInfo[] infos = _windows[id].GetType().GetFields(BindingFlags.Public | BindingFlags.Instance);
        GUILayout.BeginVertical();

        /*
         * Move through every public attribute
         */
        foreach (FieldInfo i in infos){
            GUILayout.BeginHorizontal();

            /*
             * Draw the attribute name
             */
            if(i.Name != "ChildNode" && i.Name != "ChildNodes" && i.Name != "Parent" && i.Name != "Active" && i.Name != "Collapsed")
                GUILayout.Label(i.Name);
            
            if (i.Name == "ChildNode" || i.Name == "ChildNodes" || i.Name == "Parent" || i.Name == "Active" || i.Name == "Collapsed")
            {
                /*
                 * Do nothing
                 */
            }
            else if (i.FieldType == typeof(String))
            {
                #region String
                /*
                 * Generate a textfield for this string attribute
                 */

                string value = GUILayout.TextField((string) i.GetValue(_windows[id]), 20);

                if (value != (string) i.GetValue(_windows[id]))
                    changed = true;

                i.SetValue(_windows[id], value);

                if (i.Name == "Name")
                    _windows[id].name = value;
                #endregion
            }
            else if (i.FieldType == typeof(int))
            {
                #region Int
                /*
                 * Generate a intfield for this int attribute
                 */

                int value = EditorGUILayout.IntField((int) i.GetValue(_windows[id]));

                if (value != (int) i.GetValue(_windows[id]))
                    changed = true;

                i.SetValue(_windows[id], value);
                #endregion
            }
            else if (i.FieldType == typeof(float))
            {
                #region Float
                /*
                 * Generate a floatfield for this float attribute
                 */

                float value = EditorGUILayout.FloatField((float) i.GetValue(_windows[id]));

                if (value != (float) i.GetValue(_windows[id]))
                    changed = true;

                i.SetValue(_windows[id], value);
                #endregion
            }
            else if (i.FieldType == typeof (bool))
            {
                #region Bool
                /*
                 * Generate a toggle for this bool attribute
                 */

                bool value = GUILayout.Toggle((bool) i.GetValue(_windows[id]), "");

                if (value != (bool) i.GetValue(_windows[id]))
                    changed = false;

                i.SetValue(_windows[id], value);
                #endregion
            }
            else if (i.FieldType.IsEnum)
            {
                #region Enum
                /*
                 * Generate a Popup for this enum
                 */

                int underlying = (int) Convert.ChangeType(i.GetValue(_windows[id]), Enum.GetUnderlyingType(i.FieldType));
                string[] names = Enum.GetNames(i.FieldType);
                int selected = EditorGUILayout.Popup(underlying, names);

                if (selected != (int) Convert.ChangeType(i.GetValue(_windows[id]), Enum.GetUnderlyingType(i.FieldType)))
                    changed = true;

                i.SetValue(_windows[id], selected);
                #endregion
            }
            else
            {
                #region Object
                /* 
                 * Generate a objectfield for this unknown type
                 */

                i.SetValue(_windows[id],
                    EditorGUILayout.ObjectField((UnityEngine.Object) i.GetValue(_windows[id]), i.FieldType, true));
                #endregion
            }

            GUILayout.EndHorizontal();      
        }

        /*
             * if any value has changed -> save
             */
        if (changed)
        {
            EditorUtility.SetDirty(_windows[id]);
            SaveTree();
        }
        GUILayout.EndVertical();

        #endregion

        #region Context Menu

        /*
         * If a rightclick on this window happens -> open context menu
         */
        Event e = Event.current;
        if (e.type == EventType.MouseDown && e.button == 1)
        {
            /*
             * Get all classes that derive from Node
             */
            var type = typeof(Node);
            var types =
                AppDomain.CurrentDomain.GetAssemblies()
                    .SelectMany(s => s.GetTypes())
                    .Where(p => type.IsAssignableFrom(p));

            /* 
             * Create a new Generic Menu
             */
            GenericMenu menu = new GenericMenu();

            #region New Node
            /*
             * Only draw this part if node
             * - derives from SingleChildNode and ChildNode is not set yet
             * - derives from MultiChildNode
             */
            if (IsSingleChildNode(_windows[id]) && ((SingleChildNode)_windows[id]).ChildNode == null ||
                (IsMultiChildNode(_windows[id])))
            {
                /*
                 * Iterate through every type
                 */
                foreach (var t in types)
                {
                    /*
                     * Do not add type if abstract or interface
                     */
                    if (t.IsAbstract || t.IsInterface) continue;

                    /*
                     * Check parent type
                     */
                    Type pType = null;
                    if ((IsSingleChildNode(_windows[id])))
                        pType = typeof(SingleChildNode);
                    else if (IsMultiChildNode(_windows[id]))
                        pType = typeof(MultiChildNode);

                    /*
                     * Add type as "addable" to the menu
                     */
                    menu.AddItem(new GUIContent("Add " + t), false, ContextMenuAddItem, new NewNodeInfo { T = t, Parent = _windows[id], ParentType = pType });
                }

                menu.AddSeparator("");
            }
            #endregion

            #region Remove Node

            /*
             * Do not draw this if the current Node is the root node
             */
            if (_windows[id] != _tree)
                menu.AddItem(new GUIContent("Remove Node"), false, ContextMenuRemoveItem, new DeleteNodeInfo() { Child = _windows[id], Parent = _windows[id].Parent });
            #endregion
            menu.ShowAsContext();
            e.Use();
        }

        #endregion

        GUI.DragWindow();
    }
    
    /// <summary>
    /// Draws a curve between to Windows to represent a parent-child connection
    /// </summary>
    /// <param name="start">Rect of the parent node</param>
    /// <param name="end">Rect of the child node</param>
    private void DrawNodeCurve(Rect start, Rect end)
    {
        /*
         * startPos: Bottom of parent node
         * endPos: Top of child node
         * startTan = 0
         * endTan = 0
         */
        Vector3 startPos = new Vector3(start.x + start.width / 2f, start.y + start.height, 0);
        Vector3 endPos = new Vector3(end.x + end.width / 2, end.y, 0);
        Vector3 startTan = startPos + Vector3.zero;
        Vector3 endTan = endPos + Vector3.zero;
        Color shadowCol = new Color(0, 0, 0, 0.06f);
        for (int i = 0; i < 3; i++) // Draw a shadow
            Handles.DrawBezier(startPos, endPos, startTan, endTan, shadowCol, null, (i + 1) * 5);
        Handles.DrawBezier(startPos, endPos, startTan, endTan, Color.black, null, 1);
    }
    #endregion

    #region Helpers
    /// <summary>
    /// Calculates the width for every Node in the Tree
    /// </summary>
    private void CalculateWidths(){
        /*
         * Reset dictionary
         */
        _widthTable = new Dictionary<Node, int>();

        /*
         * Start recursive call with root node
         */
        GetWidth(_tree);
    }

    /// <summary>
    /// Calculates a specific Node width by calculating its child nodes (if decorator node)
    /// </summary>
    /// <param name="n">The node you whant the width from</param>
    /// <returns>The required width of the node</returns>
    private int GetWidth(Node n)
    {
        /*
         * If node is null -> return
         */
        if (n == null) return 0;

        #region SingleChildNode
        if (IsSingleChildNode(n))
        {
            /* 
             * If theres no child -> hanlde as leaf
             */
            if (((SingleChildNode)n).ChildNode == null)
            {
                _widthTable.Add(n, (int)_windowSize.width);
                return (int)_windowSize.width;
            }

            /*
             * If collapsed -> do not calc child node
             */
            if (n.Collapsed)
            {
                _widthTable.Add(n, (int)_windowSize.width);
                return (int)_windowSize.width;
            }

            /*
             * Width = Width of Childnode
             */
            int w = GetWidth(((SingleChildNode)n).ChildNode);
            _widthTable.Add(n, w);
            return w;
        }
        #endregion

        #region MultiChildNode
        if (IsMultiChildNode(n))
        {
            /* 
             * If theres no child -> hanlde as leaf
             */
            if (((MultiChildNode)n).ChildNodes == null || ((MultiChildNode)n).ChildNodes.Count == 0)
            {
                _widthTable.Add(n, (int)_windowSize.width);
                return (int)_windowSize.width;
            }

            /*
             * If collapsed -> do not calc child node
             */
            if (n.Collapsed)
            {
                _widthTable.Add(n, (int)_windowSize.width);
                return (int)_windowSize.width;
            }

            /*
             * Width = width(node1) + width(node2) + width(node3)...
             */
            int c = 0;

            for (int i = 0; i < ((MultiChildNode)n).ChildNodes.Count; i++)
            {
                c += GetWidth(((MultiChildNode)n).ChildNodes[i]);
            }

            _widthTable.Add(n, c);
            return c;
        }
        #endregion

        #region Leaf
        if (IsLeaf(n))
        {
            _widthTable.Add(n, (int)_windowSize.width);
            return (int)_windowSize.width;
        }
        #endregion

        /* 
         * If node is none of these -> return
         */
        return 0;
    }

    /// <summary>
    /// Adds the node selected from context menu
    /// </summary>
    /// <param name="obj">NewNodeInfo that specifies required data for node creation</param>
    private void ContextMenuAddItem(Object obj)
    {
        /* 
         * Get new node info
         */
        NewNodeInfo info = (NewNodeInfo)obj;
        
        /*
         * Create the new node
         */
        Node n = (Node)CreateInstance(info.T);

        /*
         * Assign parent to node
         */
        n.Parent = info.Parent;

        /*
         * Assign node to parent
         */
        if (info.ParentType == typeof(SingleChildNode))
        {
            ((SingleChildNode)info.Parent).ChildNode = n;
        }
        else if (info.ParentType == typeof(MultiChildNode))
        {
            ((MultiChildNode)info.Parent).ChildNodes.Add(n);
        }

        /*
         * Set Node name
         */
        n.Name = n.GetType().ToString();
        n.name = n.GetType().ToString();

        /* 
         * Add Node to asset
         */
        AssetDatabase.AddObjectToAsset(n, AssetDatabase.GetAssetPath(_tree));
        /*
         * Set Node as Dirty
         */
        EditorUtility.SetDirty(n);
        /*
         * Save
         */
        SaveTree();
    }

    /// <summary>
    /// Removes the selected node from the tree
    /// </summary>
    /// <param name="obj">RemodeNodeInfo that holds required data for node deletion</param>
    private void ContextMenuRemoveItem(Object obj)
    {
        /* 
         * Get info
         */
        DeleteNodeInfo info = (DeleteNodeInfo)obj;

        /* 
         * Remove parent from child
         */
        if (IsSingleChildNode(info.Parent))
            ((SingleChildNode)info.Parent).ChildNode = null;
        if (IsMultiChildNode(info.Parent))
            ((MultiChildNode)info.Parent).ChildNodes.Remove(info.Child);

        /*
         * Destroy Node and Save tree
         */
        info.Parent = null;
        DestroyNode(info.Child);
        SaveTree();
    }

    /// <summary>
    /// Removes the given node
    /// </summary>
    /// <param name="n">The node to delete</param>
    private void DestroyNode(Node n){
        if (IsSingleChildNode(n)){
            if (((SingleChildNode)n).ChildNode != null){
                DestroyNode(((SingleChildNode)n).ChildNode);
                ((SingleChildNode)n).ChildNode = null;
            }
        }

        if (IsMultiChildNode(n)){
            foreach (Node cur in ((MultiChildNode) n).ChildNodes){
                DestroyNode(cur);
            }
            ((MultiChildNode) n).ChildNodes = null;
        }

        DestroyImmediate(n, true);
    }

    /// <summary>
    /// Returns the next usable WindowId
    /// </summary>
    /// <returns>Window ID</returns>
    private int GetNextWindowId()
    {
        /*
         * Get id
         */
        int nextId = _nextWindowId;

        /*
         * Increase next id by 1
         */
        _nextWindowId++;

        /*
         * Return id
         */
        return nextId;
    }

    /// <summary>
    /// Calls AssetDatabase.SaveAssets
    /// </summary>
    private void SaveTree()
    {
        if (_tree == null) return;

        AssetDatabase.Refresh();
        EditorUtility.SetDirty(_tree);
        AssetDatabase.SaveAssets();
    }

    /// <summary>
    /// Checks if the given node derives from SingleChildNode
    /// </summary>
    /// <param name="n">The node to check</param>
    /// <returns></returns>
    private bool IsSingleChildNode(Node n)
    {
        return typeof(SingleChildNode).IsAssignableFrom(n.GetType());
    }

    /// <summary>
    /// Checks if the given node derives from MultiChildNode
    /// </summary>
    /// <param name="n">The node to check</param>
    /// <returns></returns>
    private bool IsMultiChildNode(Node n)
    {
        return typeof(MultiChildNode).IsAssignableFrom(n.GetType());
    }

    /// <summary>
    /// Checks if the given node derives from Leaf
    /// </summary>
    /// <param name="n">The node to check</param>
    /// <returns></returns>
    private bool IsLeaf(Node n)
    {
        return typeof(Leaf).IsAssignableFrom(n.GetType());
    }

    /// <summary>
    /// Disable the given node
    /// </summary>
    /// <param name="n">The node to disable</param>
    private void DisableNode(Node n)
    {
        n.Active = false;

        if (IsSingleChildNode(n))
            DisableNode(((SingleChildNode)n).ChildNode);

        if (IsMultiChildNode(n))
        {
            foreach (Node child in ((MultiChildNode)n).ChildNodes)
                DisableNode(child);
        }
    }
    #endregion
}