﻿/// <summary>
/// Contains information required for deleting a node
/// </summary>
class DeleteNodeInfo
{
    /// <summary>
    /// The parent of the node to delete
    /// </summary>
    public Node Parent;

    /// <summary>
    /// The node to delete
    /// </summary>
    public Node Child;
}