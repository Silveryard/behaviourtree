﻿using System;
using System.Collections;

/// <summary>
/// This decorator returnes success and does not care about the child result
/// </summary>
[Serializable]
public class Succeeder : SingleChildNode
{
    public override IEnumerator Run()
    {
        //If ChildNode is not set -> cancel
        if (ChildNode == null) yield break;

        ChildNode.Active = true;

        //Run the ChildNode
        IEnumerator child = ChildNode.Run();

        //While the ChildNode runs do not move further
        while (child.MoveNext() && child.Current.Equals(Result.Running))
        {
            yield return Result.Running;
        }

        ChildNode.Active = false;

        //Always return success
        yield return Result.Success;
    }
}
