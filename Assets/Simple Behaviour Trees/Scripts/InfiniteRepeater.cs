﻿using System;
using System.Collections;
using System.Configuration;

/// <summary>
/// Repeats running the ChildNode forever. Usually the top most node in a behaviour tree
/// </summary>
[Serializable]
public class InfiniteRepeater : SingleChildNode
{
    public override IEnumerator Run()
    {
        //Run forever
        while (true){

            //If no ChildNode is set -> cancel
            if (ChildNode == null) yield break;

            ChildNode.Active = true;

            //Run the ChildNode
            IEnumerator child = ChildNode.Run();
            
            //While ChildNode returns Running, do not move further
            while (child.MoveNext() && child.Current.Equals(Node.Result.Running))
            {
                yield return Result.Running;
            }

            //As the Routine never ends it always will return Running
            yield return Result.Running;
            ChildNode.Active = false;
        }
    }
}
