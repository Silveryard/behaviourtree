﻿using System;

/// <summary>
/// Provides the functionality for a decorator node to hold multiple child nodes
/// </summary>
[Serializable]
public abstract class SingleChildNode : Node
{
    public Node ChildNode;
}
