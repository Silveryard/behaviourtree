﻿using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Runs every ChildNode in order. If failure is returned this decorator will stop immediatly and return failure
/// </summary>
[Serializable]
public class Sequence : MultiChildNode
{
    /// <summary>
    /// Check if ChildNodes is null
    /// </summary>
    public Sequence()
    {
        if (ChildNodes == null)
            ChildNodes = new List<Node>();
    }

    public override IEnumerator Run()
    {
        //Holds the information if a node has returned failure
        bool failed = false;

        //Run every single node. If failure is returned it will break this loop
        foreach (Node node in ChildNodes){
            node.Active = true;

            //Run the current ChildNode
            IEnumerator cur = node.Run();

            //While the ChildNode is running do not move further
            while (cur.MoveNext() && cur.Current.Equals(Result.Running))
            {
                yield return Result.Running;
            }

            //If the ChildNode returns failure, return failure and set failed = true
            if (cur.Current.Equals(Result.Failure))
            {
                yield return Result.Failure;
                failed = true;
            }

            node.Active = false;

            //If failed break this loop. Else continue with the next ChildNode
            if (failed)
                break;
            else
                yield return Result.Running;
        }

        //If every ChildNode has run and no failure got returned this decorator returnes success
        if (!failed)
            yield return Result.Success;
    }
}
