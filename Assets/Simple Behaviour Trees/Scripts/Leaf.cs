﻿using System;

/// <summary>
/// A Leaf is a node that has no child nodes. It usually contains specific written code for a behaviour logic
/// </summary>
[Serializable]
public abstract class Leaf : Node
{
}
