﻿using System;
using System.Collections;
using UnityEngine;

/// <summary>
/// Contains the Routine to run the Behaviour Tree
/// Holds a single ChildNode
/// </summary>
[Serializable]
public class BehaviourTree : SingleChildNode{

    [HideInInspector]
    public event Action Tick;

    /// <summary>
    /// The routine that handles behaviour logic
    /// </summary>
    /// <returns></returns>
    public IEnumerator RootRoutine(){
        Active = true;

        if (ChildNode == null) yield break;

        ChildNode.Active = true;

        if (Tick != null)
            Tick();

        IEnumerator cur = ChildNode.Run();

        if (Tick != null)
            Tick();

        while (cur.MoveNext() && Node.Result.Running.Equals(cur.Current)){
            if (Tick != null)
                Tick();
            
            yield return null;
        }
        ChildNode.Active = false;
        Active = false;
        if (Tick != null)
            Tick();
    }

    public bool TickEmpty(){
        return Tick == null;
    }

    public override IEnumerator Run()
    { //If no ChildNode is set -> cancel
        if (ChildNode == null) yield break;

        ChildNode.Active = true;

        //Run the ChildNode
        IEnumerator child = ChildNode.Run();

        //While ChildNode returns Running, do not move further
        while (child.MoveNext() && child.Current.Equals(Node.Result.Running))
        {
            yield return Result.Running;
        }

        //As the Routine never ends it always will return Running
        yield return Result.Running;
        ChildNode.Active = false;
    }
}