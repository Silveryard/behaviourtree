﻿using System;
using System.Collections;

/// <summary>
/// This repeater keeps calling its ChildNode until it returns the goal value
/// </summary>
[Serializable]
public class ResultRepeater : SingleChildNode{
    /// <summary>
    /// The goal the ChildNode has to return
    /// </summary>
    public Result Goal;

    public override IEnumerator Run(){
        //Run forever (break if goal is reached)
        while (true){
            //If ChildNode is not set -> cancel
            if (ChildNode == null) yield break;

            ChildNode.Active = true;

            //Run the ChildNode
            IEnumerator child = ChildNode.Run();
            
            //While the ChildNode is running, do not move further
            while (child.MoveNext() && child.Current.Equals(Result.Running)){
                yield return Result.Running;
            }

            ChildNode.Active = false;

            //If the goal is reached -> break the while. Else go for another round
            if (child.Current.Equals(Goal))
                break;
        }

        //This decorator cannot return failure. If goal isn´t reached it will go on and run forever
        yield return Result.Success;
    }
}