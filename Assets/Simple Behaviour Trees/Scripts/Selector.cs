﻿using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Runs every ChildNode until Success is returned
/// </summary>
[Serializable]
public class Selector : MultiChildNode{

    /// <summary>
    /// Check if ChildNode is null
    /// </summary>
    public Selector(){
        if(ChildNodes == null)
            ChildNodes = new List<Node>();
    }

    public override IEnumerator Run(){
        //Holds the information if the goal is reached
        bool success = false;

        //Call every single ChildNode. If goal is reached it will break this loop
        foreach (Node node in ChildNodes){

            node.Active = true;
            //Run the current ChildNode
            IEnumerator cur = node.Run();

            //While the ChildNode is running do not move further
            while (cur.MoveNext() && cur.Current.Equals(Result.Running)){
                yield return Result.Running;
            }

            //If the goal is reached, return success and set success = true
            if (cur.Current.Equals(Result.Success)){
                yield return Result.Success;
                success = true;
            }

            node.Active = false;

            //If the goal is reached break the loop. Else return running and continue with next node
            if (success)
                break;
            else
                yield return Result.Running;
        }

        //If every Node has been run and no goal is reached return failure
        if (!success)
            yield return Result.Failure;
    }
}
