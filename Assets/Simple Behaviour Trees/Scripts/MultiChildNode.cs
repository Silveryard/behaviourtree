﻿using System;
using System.Collections.Generic;

/// <summary>
/// Provides the functionality for a decorator node to hold multiple child nodes
/// </summary>
[Serializable]
public abstract class MultiChildNode : Node
{
    public List<Node> ChildNodes;
}
