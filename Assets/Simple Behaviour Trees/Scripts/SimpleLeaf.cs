﻿using System;
using System.Collections;
using UnityEngine;

/// <summary>
/// A simple leaf that returnes the selected Result. Warning: Do not return running
/// </summary>
[Serializable]
public class SimpleLeaf : Leaf{
    /// <summary>
    /// The result this 
    /// </summary>
    public Node.Result Return;

    public override IEnumerator Run(){

        yield return Result.Success;


        //Return one running to avoid freeze
        yield return Result.Running;
        //Return the result
        yield return Return;
    }
}