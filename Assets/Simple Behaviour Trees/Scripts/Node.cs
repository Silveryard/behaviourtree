﻿using System;
using System.Collections;
using UnityEngine;

/// <summary>
/// A node is a part of the behaviour tree
/// Decorator nodes do have one or more child nodes and create the behaviour flow
/// Leaf nodes no not have any child nodes and contain the low level behaviour logic
/// </summary>
[Serializable]
public abstract class Node : ScriptableObject{

    /// <summary>
    /// A running node can return 3 types of result:
    /// Success: The node has finished and succeeded in its progress
    /// Failure: The node has finished and failed in its progress
    /// Running: The node has not finished yet. It will need another call to procceed
    /// </summary>
    public enum Result{
        Success,
        Failure,
        Running
    }

    /// <summary>
    /// The name of the node
    /// </summary>
    public String Name;

   /// <summary>
   /// The parent of the node
   /// </summary>
    public Node Parent;

    /// <summary>
    /// True if this node is currently executed
    /// </summary>
    public bool Active;

    /// <summary>
    /// Says the editorscript if it should render childnodes
    /// </summary>
    public bool Collapsed;

    /// <summary>
    /// This method contains the node logic
    /// </summary>
    /// <returns></returns>
    public abstract IEnumerator Run();

    /// <summary>
    /// A abstract constructor to handle NullReferenceExceptions
    /// </summary>
    public Node(){
        Name = "";
        Active = false;
        Collapsed = false;
    }

}
