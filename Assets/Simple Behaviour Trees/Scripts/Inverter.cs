﻿using System;
using System.Collections;

/// <summary>
/// Inverts the returned result of ChildNode
/// </summary>
[Serializable]
public class Inverter : SingleChildNode
{
    public override IEnumerator Run()
    {
        //If no ChildNode is set -> cancel
        if(ChildNode == null)yield break;

        ChildNode.Active = true;

        //Run the child node
        IEnumerator child = ChildNode.Run();

        //While the child node returns Running don´t move further
        while (child.MoveNext() && child.Current.Equals(Result.Running))
        {
            yield return Result.Running;
        }

        ChildNode.Active = false;

        //Invert the Result of ChildNode
        if (child.Current.Equals(Result.Running))
            yield return Result.Failure;

        if (child.Current.Equals(Result.Failure))
            yield return Result.Success;
    }
}
