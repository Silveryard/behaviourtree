﻿using System;
using System.Collections;

/// <summary>
/// Keeps running until Value is greater or equal Goal
/// </summary>
[Serializable]
public class ValueRepeater : SingleChildNode
{
    //This value needs to get modified
    public float Value;
    //This goal has to be reached
    public float Goal;

    public override IEnumerator Run()
    {
        //Continue while the goal is not reached
        while (Value < Goal)
        {
            //If child node 
            if (ChildNode == null) yield break;
            IEnumerator child = ChildNode.Run();

            while (child.MoveNext() && child.Current.Equals(Result.Running))
            {
                yield return Result.Running;
            }
        }

        yield return Result.Success;
    }
}
